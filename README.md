pipeline {
    agent any

    stages {
        stage("Build") {
            steps {
                echo 'building the application...'
            }
        }
        
        stage("Test") {
            steps {
                echo 'testing the application...'
            }
        }
        
        stage("deploy") {
            steps {
                echo 'deploying the application...'
                
            }
        }
    }
}
